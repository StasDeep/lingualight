FROM mysql:5.7

ENV MYSQL_DATABASE lingualight
ENV MYSQL_ROOT_PASSWORD=docker

COPY ./lingualight/migrations/in.sql /docker-entrypoint-initdb.d/
