# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from datetime import datetime, timedelta

import bcrypt
import requests
from django.db import connection
from django.http import Http404, JsonResponse
from django.shortcuts import redirect
from django.views import View
from django.views.generic import FormView, TemplateView, CreateView

from lingualight.forms import LoginForm, SignupForm
from lingualight.models import Users, Texts, Words, WordTranslations, UserWordTranslations, Comments
from lingualight.queries import LOGGING_QUERY


class HomeView(TemplateView):
    template_name = 'lingualight/home.html'

    def get_context_data(self, **kwargs):
        context = super(HomeView, self).get_context_data(**kwargs)
        context['user'] = self.request.user
        return context


class LoginView(FormView):
    template_name = 'lingualight/login.html'
    form_class = LoginForm

    def form_valid(self, form):
        session_key = form.get_session_key()

        response = redirect('home')
        response.set_cookie('session_token', session_key)
        return response


class LogoutView(View):

    def post(self, request):
        if request.session_key is not None:
            with connection.cursor() as cursor:
                cursor.execute('DELETE FROM sessions '
                               'WHERE `key` = %s;', [request.session_key])

        response = redirect('home')
        response.delete_cookie('session_token')
        return response


class SignupView(FormView):
    template_name = 'lingualight/signup.html'
    form_class = SignupForm

    def form_valid(self, form):
        email = form.cleaned_data['email']
        password = bytes(form.cleaned_data['password'].encode('latin-1'))
        hashed_password = bcrypt.hashpw(password, bcrypt.gensalt())
        language_id = form.cleaned_data['language_id']

        with connection.cursor() as cursor:
            cursor.execute('INSERT INTO users (`email`, `password`, `is_admin`, `language_id`) '
                           'VALUES (%s, %s, %s, %s);', [email, hashed_password, 0, language_id])

        response = redirect('home')
        return response


class TextView(TemplateView):
    template_name = 'lingualight/texts.html'

    def get_context_data(self, **kwargs):
        context = super(TextView, self).get_context_data(**kwargs)
        context['texts'] = list(Texts.objects.raw('SELECT t.id, t.title, t.created_by_id, ct.user_id '
                                                  'FROM texts t '
                                                  'LEFT JOIN completed_texts ct ON t.id = ct.text_id '
                                                  'AND ct.user_id = %s '
                                                  'ORDER BY t.id DESC;', [self.request.user.id]))
        context['user'] = self.request.user
        return context


class SingleTextView(TemplateView):
    template_name = 'lingualight/text.html'

    def get_context_data(self, **kwargs):
        context = super(SingleTextView, self).get_context_data(**kwargs)
        texts = list(Texts.objects.raw(
            'SELECT t.id, t.content, t.title, ct.user_id '
            'FROM texts t '
            'LEFT JOIN completed_texts ct ON t.id = ct.text_id '
            'AND ct.user_id = %s '
            'WHERE t.id = %s;',
            [self.request.user.id, self.kwargs['pk']]
        ))

        if len(texts) == 0:
            raise Http404()

        context['text'] = texts[0]
        context['paragraphs'] = [p.split() for p in texts[0].content.split('\n')]

        context['comments'] = list(Comments.objects.raw(
            'SELECT c.id, c.reply_to_id, c.user_id, u.email '
            'FROM comments c '
            'JOIN users u ON u.id = c.user_id '
            'WHERE c.text_id = %s;',
            [context['text'].id]
        ))

        return context


class CreateTextView(CreateView):
    model = Texts
    template_name = 'lingualight/create-text.html'
    fields = ['title', 'content']

    def post(self, request, *args, **kwargs):
        data = self.get_form().data

        with connection.cursor() as cursor:
            cursor.execute('INSERT INTO texts (`title`, `content`, `created_by_id`) '
                           'VALUES (%s, %s, %s);', [data['title'], data['content'], request.user.id])

        return redirect('texts')


class CommentView(View):

    def post(self, request, *args, **kwargs):
        content = request.POST['content']
        text_id = int(request.POST['text_id'])
        if request.POST['reply_to_id']:
            reply_to_id = int(request.POST['reply_to_id'])
        else:
            reply_to_id = None

        with connection.cursor() as cursor:
            cursor.execute('INSERT INTO comments (`content`, `user_id`, `text_id`, `reply_to_id`) '
                           'VALUES (%s, %s, %s, %s);', [content, request.user.id, text_id, reply_to_id])

        return redirect('text', pk=text_id)


class TranslationView(View):

    def get(self, request):
        url = 'https://translate.yandex.net/api/v1.5/tr.json/translate?key={key}&text={word}&lang={lang}'
        key = 'trnsl.1.1.20171122T195203Z.d1951ee3c9e6a4c3.0490ebf2bacfced4f920c76398474fdd55d7d3e8'
        word = request.GET['word']
        lang = request.user.language_id.lower()

        yandex_translations = requests.get(url.format(**locals())).json()['text']
        with connection.cursor() as cursor:
            cursor.execute(
                'SELECT tr.word, uwt.user_id '
                'FROM word_translations wt '
                'INNER JOIN words w ON wt.word_id = w.id AND w.word = %s '
                'INNER JOIN words tr ON wt.translation_id = tr.id AND tr.language_id = %s '
                'LEFT JOIN user_word_translations uwt ON uwt.word_translation_id = wt.id AND uwt.user_id = %s;',
                [word, lang.upper(), request.user.id])

            rows = cursor.fetchall()
            db_translations = [row[0] for row in rows]
            user_translations = dict(rows)

        translations = [{
            'translation': translation,
            'already': user_translations.get(translation) is not None
        } for translation in list(set(yandex_translations + db_translations))]

        return JsonResponse({'translations': translations})

    def post(self, request):
        word = self.get_or_create_word(request.POST['word'], 'EN')
        translation = self.get_or_create_word(request.POST['translation'], request.user.language_id)
        word_translation = self.get_or_create_word_translation(word, translation)
        last_trained = datetime.now() - timedelta(weeks=50)

        with connection.cursor() as cursor:
            cursor.execute('INSERT INTO user_word_translations (completeness, last_trained, word_translation_id, user_id) '
                           'VALUES (0, %s, %s, %s);', [last_trained, word_translation.id, request.user.id])

        return JsonResponse(dict())

    def get_or_create_word(self, word, lang_id):
        words = Words.objects.raw('SELECT * FROM words WHERE word = %s AND language_id = %s;', [word, lang_id])

        if len(list(words)) == 0:
            with connection.cursor() as cursor:
                cursor.execute('INSERT INTO words (`word`, `language_id`) '
                               'VALUES (%s, %s);', [word, lang_id])

            words = Words.objects.raw('SELECT * FROM words WHERE id = %s;', [cursor.lastrowid])

        return words[0]

    def get_or_create_word_translation(self, word, translation):
        word_translations = WordTranslations.objects.raw('SELECT * FROM word_translations '
                                                         'WHERE word_id = %s '
                                                         'AND translation_id = %s;', [word.id, translation.id])

        if len(list(word_translations)) == 0:
            with connection.cursor() as cursor:
                cursor.execute('INSERT INTO word_translations (`word_id`, `translation_id`) '
                               'VALUES (%s, %s);', [word.id, translation.id])

            word_translations = WordTranslations.objects.raw('SELECT * FROM word_translations '
                                                             'WHERE id = %s;', [cursor.lastrowid])

        return word_translations[0]


class CompleteTextView(View):

    def post(self, request, *args, **kwargs):
        with connection.cursor() as cursor:
            cursor.execute('INSERT INTO completed_texts (completed_at, text_id, user_id) '
                           'VALUES (%s, %s, %s);', [datetime.now(), kwargs['pk'], request.user.id])

        return JsonResponse(dict())


class TrainingView(TemplateView):
    template_name = 'lingualight/training.html'


class TrainingWordsView(View):

    def get(self, request):
        words = []

        words = UserWordTranslations.objects.raw('SELECT uwt.id, w.word, t.word translation '
                                                 'FROM user_word_translations uwt '
                                                 'INNER JOIN word_translations wt ON uwt.word_translation_id = wt.id '
                                                 'INNER JOIN words w ON wt.word_id = w.id '
                                                 'INNER JOIN words t ON wt.translation_id = t.id '
                                                 'WHERE uwt.user_id = %s AND uwt.completeness < 10 '
                                                 'ORDER BY uwt.last_trained ASC '
                                                 'LIMIT 5;', [request.user.id])

        words = [{'id': word.id, 'word': word.word, 'translation': word.translation}
                 for word in words]

        return JsonResponse({'data': words})

    def post(self, request, *args, **kwargs):
        right_translations = request.POST['rightTranslations']

        if right_translations:
            uwt_ids = [int(uwt_id) for uwt_id in right_translations.split(',')]
        else:
            return JsonResponse(dict())

        with connection.cursor() as cursor:
            cursor.execute('UPDATE user_word_translations uwt '
                           'SET completeness = CASE WHEN completeness >= 10 THEN 10 ELSE completeness + 1 END, '
                           '  last_trained = CURTIME() '
                           'WHERE id IN %s;', [uwt_ids])

        return JsonResponse(dict())


class DictionaryView(TemplateView):
    template_name = 'lingualight/dictionary.html'

    def get_context_data(self, **kwargs):
        context = super(DictionaryView, self).get_context_data(**kwargs)

        word_translations = UserWordTranslations.objects.raw(
            'SELECT uwt.id, w.word, t.word translation, uwt.completeness '
            'FROM user_word_translations uwt '
            'INNER JOIN word_translations wt ON uwt.word_translation_id = wt.id '
            'INNER JOIN words w ON wt.word_id = w.id '
            'INNER JOIN words t ON wt.translation_id = t.id '
            'WHERE uwt.user_id = %s '
            'ORDER BY uwt.completeness ASC;', [self.request.user.id])

        context['word_translations'] = list(word_translations)

        return context


class LoggingView(TemplateView):
    template_name = 'lingualight/logging.html'

    def get_context_data(self, **kwargs):
        context = super(LoggingView, self).get_context_data(**kwargs)

        with connection.cursor() as cursor:
            cursor.execute(LOGGING_QUERY)
            rows = cursor.fetchall()

        context['log_lines'] = rows

        return context
