from django.conf.urls import url
from django.views.generic import RedirectView

from lingualight import views

urlpatterns = [
    url(r'^home/$',                         views.HomeView.as_view(),                   name='home'),
    url(r'^signup/$',                       views.SignupView.as_view(),                 name='signup'),
    url(r'^login/$',                        views.LoginView.as_view(),                  name='login'),
    url(r'^logout/$',                       views.LogoutView.as_view(),                 name='logout'),
    url(r'^texts/$',                        views.TextView.as_view(),                   name='texts'),
    url(r'^dictionary/$',                   views.DictionaryView.as_view(),             name='dictionary'),
    url(r'^logging/$',                      views.LoggingView.as_view(),                name='logging'),
    url(r'^training/$',                     views.TrainingView.as_view(),               name='training'),
    url(r'^training/words/$',               views.TrainingWordsView.as_view(),          name='training-words'),
    url(r'^texts/(?P<pk>\d+)/$',            views.SingleTextView.as_view(),             name='text'),
    url(r'^texts/(?P<pk>\d+)/complete/$',   views.CompleteTextView.as_view(),           name='complete-text'),
    url(r'^texts/create/$',                 views.CreateTextView.as_view(),             name='create-text'),
    url(r'^translation/$',                  views.TranslationView.as_view(),            name='translation'),
    url(r'^comments/$',                     views.CommentView.as_view(),                name='create-comment'),
    url(r'^$',                              RedirectView.as_view(pattern_name='home'),  name='index'),
]
