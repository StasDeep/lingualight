SET collation_connection = 'utf8_general_ci';
ALTER DATABASE lingualight CHARACTER SET utf8 COLLATE utf8_general_ci;

SET FOREIGN_KEY_CHECKS = 0;

-- DROP TABLE IF EXISTS `comments`;
-- DROP TABLE IF EXISTS `completed_texts`;
-- DROP TABLE IF EXISTS `languages`;
-- DROP TABLE IF EXISTS `sessions`;
-- DROP TABLE IF EXISTS `texts`;
-- DROP TABLE IF EXISTS `users`;
-- DROP TABLE IF EXISTS `words`;
-- DROP TABLE IF EXISTS `word_translations`;
-- DROP TABLE IF EXISTS `user_word_translations`;


CREATE TABLE `languages` (
  `code` varchar(2) NOT NULL,
  PRIMARY KEY (`code`)
);


CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `is_admin` tinyint(1) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `language_id` varchar(2) NOT NULL,
  `created_at` datetime(6) NOT NULL DEFAULT NOW(6),
  PRIMARY KEY (`id`),
  FOREIGN KEY (`language_id`) REFERENCES `languages` (`code`)
);


CREATE TABLE `sessions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
);


CREATE TABLE `words` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `word` varchar(255) NOT NULL,
  `language_id` varchar(2) NOT NULL,
  PRIMARY KEY (`id`),
  FOREIGN KEY (`language_id`) REFERENCES `languages` (`code`)
);


CREATE TABLE `word_translations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `translation_id` int(11) NOT NULL,
  `word_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  FOREIGN KEY (`translation_id`) REFERENCES `words` (`id`),
  FOREIGN KEY (`word_id`) REFERENCES `words` (`id`)
);


CREATE TABLE `user_word_translations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `completeness` int(10) unsigned NOT NULL,
  `last_trained` datetime(6) NOT NULL,
  `word_translation_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` datetime(6) NOT NULL DEFAULT NOW(6),
  PRIMARY KEY (`id`),
  FOREIGN KEY (`word_translation_id`) REFERENCES `word_translations` (`id`),
  FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
);


CREATE TABLE `texts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content` longtext NOT NULL,
  `title` varchar(255) NOT NULL,
  `created_by_id` int(11) NOT NULL,
  `created_at` datetime(6) NOT NULL DEFAULT NOW(6),
  PRIMARY KEY (`id`),
  FOREIGN KEY (`created_by_id`) REFERENCES `users` (`id`)
);


CREATE TABLE `completed_texts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `completed_at` datetime(6) NOT NULL,
  `text_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  FOREIGN KEY (`text_id`) REFERENCES `texts` (`id`),
  FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
);

CREATE TABLE `comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content` longtext NOT NULL,
  `text_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `reply_to_id` int(11) NULL,
  `created_at` datetime(6) NOT NULL DEFAULT NOW(6),
  PRIMARY KEY (`id`),
  FOREIGN KEY (`text_id`) REFERENCES `texts` (`id`),
  FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  FOREIGN KEY (`reply_to_id`) REFERENCES `comments` (`id`)
);

INSERT INTO languages (`code`)
VALUES ('RU'), ('EN'), ('ES'), ('FR');

INSERT INTO users (`is_admin`, `email`, `password`, `language_id`)
VALUES (1, 'admin@lingualight.com', '$2b$12$P3lKJhqEh0SR6Lbz8FmAkeGjEe4PnLpedlo0jZOC2pmKz7/2pXNkO', 'RU');

ALTER TABLE words CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;

SET FOREIGN_KEY_CHECKS = 1;
