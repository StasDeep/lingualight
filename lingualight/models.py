from __future__ import unicode_literals

from django.db import models


class Comments(models.Model):
    content = models.TextField()
    text = models.ForeignKey('Texts', models.DO_NOTHING)
    user = models.ForeignKey('Users', models.DO_NOTHING)
    reply_to = models.ForeignKey('self', models.DO_NOTHING, blank=True, null=True)
    created_at = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'comments'


class CompletedTexts(models.Model):
    completed_at = models.DateTimeField()
    text = models.ForeignKey('Texts', models.DO_NOTHING)
    user = models.ForeignKey('Users', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'completed_texts'


class Languages(models.Model):
    code = models.CharField(primary_key=True, max_length=2)

    class Meta:
        managed = False
        db_table = 'languages'


class Sessions(models.Model):
    key = models.CharField(max_length=255)
    user = models.ForeignKey('Users', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'sessions'


class Texts(models.Model):
    content = models.TextField()
    created_by = models.ForeignKey('Users', models.DO_NOTHING)
    title = models.CharField(max_length=255)
    created_at = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'texts'


class Words(models.Model):
    word = models.CharField(max_length=255)
    language = models.ForeignKey(Languages, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'words'


class WordTranslations(models.Model):
    translation = models.ForeignKey('Words', models.DO_NOTHING, related_name='as_translation')
    word = models.ForeignKey('Words', models.DO_NOTHING, related_name='as_word')

    class Meta:
        managed = False
        db_table = 'word_translations'


class UserWordTranslations(models.Model):
    completeness = models.IntegerField()
    last_trained = models.DateTimeField()
    word_translation = models.ForeignKey('WordTranslations', models.DO_NOTHING)
    user = models.ForeignKey('Users', models.DO_NOTHING)
    created_at = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'user_word_translations'


class Users(models.Model):
    is_admin = models.IntegerField()
    email = models.CharField(max_length=255)
    password = models.CharField(max_length=255)
    language = models.ForeignKey(Languages, models.DO_NOTHING)
    created_at = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'users'
