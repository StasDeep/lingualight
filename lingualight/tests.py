# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import uuid

import bcrypt

from django.db import connection
from django.http import SimpleCookie
from django.test import TestCase, Client
from django.urls import reverse


class TextViewTests(TestCase):

    def setUp(self):
        email = 'test@test.com'
        password = '123123'.encode('latin-1')
        hashed_password = bcrypt.hashpw(password, bcrypt.gensalt())
        language_id = 'RU'

        with connection.cursor() as cursor:
            cursor.execute('INSERT INTO users (`email`, `password`, `is_admin`, `language_id`) '
                           'VALUES (%s, %s, %s, %s);', [email, hashed_password, 0, language_id])

        generated_key = uuid.uuid4()

        with connection.cursor() as cursor:
            cursor.execute('INSERT INTO sessions (`key`, user_id) '
                           'VALUES (%s, %s);', [generated_key, 1])

        cookies = SimpleCookie()
        cookies["session_token"] = generated_key
        self.client = Client(HTTP_COOKIE=cookies.output(header='', sep='; '))

    def test_existing_text(self):
        with connection.cursor() as cursor:
            cursor.execute('INSERT INTO texts (`title`, `content`, `created_by_id`) '
                           'VALUES (%s, %s, %s);', ['Test', 'Content', 1])

        url = reverse('text', args=(1,))
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_inexistent_test(self):
        url = reverse('text', args=(123,))
        response = self.client.get(url)
        self.assertEqual(response.status_code, 404)
