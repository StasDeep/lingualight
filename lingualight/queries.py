LOGGING_QUERY = '''
    SELECT
      u.email as user,
      t.msg,
      t.created_at
    FROM (
      SELECT
        created_by_id AS user_id,
        CONCAT('Added text "', title, '".') as msg,
        created_at
      FROM texts
    
      UNION
    
      SELECT
        id AS user_id,
        'User created.' as msg,
        created_at
      FROM users
    
      UNION
    
      SELECT
        uwt.user_id,
        CONCAT('Added word translation: ', w.word, ' - ', t.word, '.') as msg,
        uwt.created_at
      FROM user_word_translations AS uwt
      JOIN word_translations AS wt ON uwt.word_translation_id = wt.id
      JOIN words AS w ON wt.word_id = w.id
      JOIN words AS t ON wt.translation_id = t.id
    
      UNION
    
      SELECT
        ct.user_id,
        CONCAT('Completed text "', t.title, '".') as msg,
        completed_at AS created_at
      FROM completed_texts as ct
      JOIN texts AS t ON ct.text_id = t.id
    ) AS t
    JOIN users AS u ON t.user_id = u.id
    ORDER BY created_at DESC;
'''
