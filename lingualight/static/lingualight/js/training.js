$(document).ready(function () {

    let pairs = [];

    $('#startTraining').click(function () {
        $.get('http://127.0.0.1:8000/training/words/').done(function (response) {
            pairs = response.data;

            $('#startTraining').hide();
            showTraining();
        });
    });

    function showTraining() {
        let resultHtml = '';
        for (let pair of pairs) {
            resultHtml += '<div class="row">' +
                            '<div class="col-xs-3"><span class="pull-right">' + pair.translation + '</span></div>' +
                            '<div class="col-xs-4"><div class="form-group"><input class="form-control" id="pair-' + pair.id + '"></div></div>' +
                          '</div>'
        }

        $('#trainingContainer').prepend(resultHtml);
        $('#trainingContainer').show();
    }

    $('#submitTraining').click(function () {
        let rightTranslations = pairs.filter(p => p.word.toLowerCase() === $('#pair-' + p.id).val().toLowerCase()).map(p => p.id).join(',');
        $.post('http://127.0.0.1:8000/training/words/', {rightTranslations: rightTranslations}).done(function () {
            $('#submitTraining').remove();
            let resultHtml = '';
            for (let pair of pairs) {
                let rightClass = pair.word.toLowerCase() === $('#pair-' + pair.id).val().toLowerCase() ? 'right' : 'not-right';
                resultHtml += '<div class="row">' +
                                '<div class="col-xs-3"><span class="pull-right">' + pair.translation + '</span></div>' +
                                '<div class="col-xs-4"><div class="form-group word-' + rightClass + '">' + pair.word + '</div></div>' +
                              '</div>'
            }
            $('#trainingContainer').html(resultHtml);
        });
    });
});
