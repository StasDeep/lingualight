$(document).ready(function () {
    let $popup = $('#popup');
    let selectedWord = '';

    $('body').click(function () {
        $popup.hide();
    });

    function addCustomTranslation() {
        let translation = $('#customTranslation').val();
        if (translation.length) {
            addTranslation(translation).done(function () {
                $popup.find($('.translation')).last().after('<p class="translation already">' + translation + '</p>');
                $('#customTranslation').val('');
                $popup.fadeOut();
            });
        }
    }

    $('#customTranslation').keyup(function (event) {
        if (event.originalEvent.key === 'Enter') {
            addCustomTranslation();
        }
    });

    $('#addTranslation').click(addCustomTranslation);

    function addTranslation(translation) {
        return $.post('http://127.0.0.1:8000/translation/', {
            word: selectedWord,
            translation: translation
        });
    }

    $popup.click(function (event) {
        let elem  = $(event.target);
        if (elem.hasClass('translation')) {
            addTranslation(elem.text()).done(function () {
                elem.addClass('already');
                $popup.fadeOut();
            });
        }
        event.stopPropagation();
    });

    $('.text-word').click(function (event) {
        let elem = $(event.target);
        selectedWord = elem.text().toLowerCase().replace(/[”“.,\/#!$%\^&\*;:{}=\-_`~()]/g, '');

        $.getJSON('http://127.0.0.1:8000/translation/?word='+selectedWord, function (data) {
            let translations = data.translations;
            let translationsHtml = '';

            $popup.find($('.translation')).remove();

            for (let translation of translations) {
                let className = 'translation';
                if (translation.already) {
                    className += ' already'
                }
                translationsHtml += '<p class="' + className + '">' + translation.translation + '</p>';
            }
            $popup.prepend(translationsHtml);
            let popupHeight = $popup.css('height');
            popupHeight = parseInt(popupHeight.substr(0, popupHeight.length - 2));
            $popup.css('top', elem[0].offsetTop - popupHeight - 5);
            $popup.css('left', elem[0].offsetLeft);
            $popup.show();
        });
    });

    $('#mark-as-completed').click(function () {
        let textId = $('#textId').val();
        $.post('http://127.0.0.1:8000/texts/' + textId + '/complete/').done(function () {
            $('#mark-as-completed').hide();
            $('#text-is-completed').show();
        });
    });
});

function setReply(commentId) {
    $('input[name=reply_to_id]').val(commentId);
    $('#replylabel').text(' (reply to #' + commentId + ')');
}

function removeReply() {
    $('input[name=reply_to_id]').val('');
    $('#replylabel').text('');
}
