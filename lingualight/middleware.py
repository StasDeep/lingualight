from lingualight.models import Users


class UserMiddleware(object):
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        delete_session_key = False

        session_key = request.COOKIES.get('session_token')

        if session_key is None:
            request.user = None
        else:
            users = Users.objects.raw('SELECT * '
                                      'FROM users '
                                      'INNER JOIN sessions ON sessions.user_id = users.id '
                                      'WHERE sessions.key = %s '
                                      'LIMIT 1;', [session_key])

            if len(list(users)) == 0:
                request.user = None
                delete_session_key = True
            else:
                request.user = users[0]
                request.session_key = session_key

        response = self.get_response(request)

        if delete_session_key:
            response.delete_cookie('session_token')

        return response
