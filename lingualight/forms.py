import uuid

import bcrypt
from django import forms
from django.core.exceptions import ValidationError
from django.db import connection

from lingualight.models import Users


class LoginForm(forms.Form):
    email = forms.CharField()
    password = forms.CharField(widget=forms.PasswordInput)

    def clean(self):
        cleaned_data = super(LoginForm, self).clean()

        email = cleaned_data['email']
        password = cleaned_data['password']

        users = Users.objects.raw('SELECT * '
                                  'FROM users '
                                  'WHERE email = %s '
                                  'LIMIT 1;', [email])

        if len(list(users)) == 0:
            raise ValidationError('Wrong email or password')

        user = users[0]
        if not bcrypt.checkpw(bytes(password.encode('latin-1')),
                              bytes(user.password.encode('latin-1'))):
            raise ValidationError('Wrong email or password')

        cleaned_data['user'] = user

        return cleaned_data

    def get_session_key(self):
        user = self.cleaned_data['user']

        generated_key = uuid.uuid4()

        with connection.cursor() as cursor:
            cursor.execute('INSERT INTO sessions (`key`, user_id) '
                           'VALUES (%s, %s);', [generated_key, user.id])

        return generated_key


class SignupForm(forms.Form):
    email = forms.CharField()
    password = forms.CharField(widget=forms.PasswordInput, min_length=6)
    language_id = forms.CharField(max_length=2,
                                  label='Language',
                                  widget=forms.Select(choices=[('RU', 'Russian'),
                                                               ('ES', 'Spanish'),
                                                               ('FR', 'French')]))

    def clean(self):
        cleaned_data = super(SignupForm, self).clean()

        email = cleaned_data['email']
        password = cleaned_data['password']

        users = Users.objects.raw('SELECT * '
                                  'FROM users '
                                  'WHERE email = %s '
                                  'LIMIT 1;', [email])

        if len(list(users)) != 0:
            raise ValidationError('Email is taken')

        return cleaned_data
